package Owner.Mesa;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class SignupPage extends AndroidCap
{
	public static void main(String[] args) throws MalformedURLException, InterruptedException
	{
		AndroidDriver<AndroidElement> driver=Capabilities();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(9000);
		
		driver.findElementByXPath("//android.widget.TextView[@text='Sign Up']").click();//Sign Up Button
		//Sign-Up Screen.
		//driver.findElementByXPath("//*[@class='android.widget.Button'and @bounds='[55,1533][1025,1656]']").click();
                driver.findElementByXPath("//*[@class='android.view.ViewGroup'and @bounds='[392,358][689,740]']").click();
                driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();//Permission Button
                driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
                //driver.findElementByXPath("//android.widget.Button[@text='ALLOW']").click();
                Thread.sleep(9000);
                //Image Uplodaing
                
                driver.findElementByXPath("//android.widget.TextView[@text='images (1).jpeg']").click();
                
                driver.findElementById("com.mesa:id/menu_crop").click();//Crop Window.
                Thread.sleep(6000);
                
                
	        driver.findElementByXPath("//android.widget.EditText[@text='Enter First Name And Last Name']").sendKeys("Rajeev Singh");
	        driver.findElementByXPath("//android.widget.EditText[@text='Enter Email Address']").sendKeys("qwqw@yopmail.com");
	        driver.findElementByXPath("//android.widget.EditText[@text='Enter Mobile Number']").sendKeys("7932932877");
	        driver.findElementByXPath("//android.widget.EditText[@text='Enter Password']").sendKeys("qwerty");
	        driver.findElementByXPath("//android.widget.TextView[@text='Sign Up']").click();
	        Thread.sleep(6000);
	        //SMS Validation
	        
	        driver.findElementByName("android.widget.EditText").sendKeys("1234");
	        driver.findElementByXPath("//android.widget.TextView[@text='Continue']").click();
	        
	        //Add Documents
	        
	        driver.findElementByXPath("//*[@class='android.view.ViewGroup'and @bounds='[934,600][978,644]']").click();
	        Thread.sleep(6000);
	        driver.findElementByXPath("//android.widget.TextView[@text='images (1).jpeg']").click();//1st image.
	        
	        driver.findElementByXPath("//*[@class='android.view.ViewGroup'and @bounds='[934,899][978,943]']").click();
	        Thread.sleep(6000);
	        driver.findElementByXPath("//android.widget.TextView[@text='c7c1e0022bb4217a2e043d534ccdc1e4.jpg']").click();//Secound Image
	        
	        driver.findElementByXPath("//*[@class='android.view.ViewGroup'and @bounds='[934,1198][978,1242]']").click();
	        Thread.sleep(6000);
	        driver.findElementByXPath("//android.widget.TextView[@text='images (1).jpeg']").click();//3rd Image
	        
	        driver.findElementByXPath("//*[@class='android.view.ViewGroup'and @bounds='[934,1497][978,1541]']").click();
	        Thread.sleep(6000);
	        driver.findElementByXPath("//android.widget.TextView[@text='c7c1e0022bb4217a2e043d534ccdc1e4.jpg']").click();//4th Image
	        
	        driver.findElementByXPath("//android.widget.TextView[@text='Continue']").click();
	}
}
